import React from "react";
import Header from "./Components/Header";
import "./App.css";
import ToDoForm from "./Components/ToDoForm";

function App() {
  return (
    <div className="App">
      <Header />
      <ToDoForm />
    </div>
  );
}

export default App;
