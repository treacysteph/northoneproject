import React from "react";
import { AppBar, makeStyles, Typography, Toolbar } from "@material-ui/core";

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});

const Header = () => {
  const classes = useStyles();

  return (
    <div>
      <AppBar position="static">
        <Toolbar>
          <Typography className={classes.title} variant="h6" noWrap>
            To-Do App
          </Typography>
        </Toolbar>
      </AppBar>
    </div>
  );
};

export default Header;
