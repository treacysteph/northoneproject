import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { addTodo } from "../Redux/todoListSlice";
import { closeModal } from "../Redux/modalSlice";
import {
  TextField,
  Select,
  Button,
  Grid,
  InputLabel,
  MenuItem,
  FormControl,
  makeStyles,
} from "@material-ui/core";
import DateFnsUtils from "@date-io/date-fns";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from "@material-ui/pickers";

function getModalStyle() {
  const top = 50;
  return {
    top: `${top}%`,
    left: `${top}%`,
    transform: `translate(-${top}%, -${top}%)`,
  };
}

const useStyles = makeStyles((theme) => ({
  paper: {
    position: "absolute",
    width: 600,
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(2, 4, 3),
  },
}));

const ToDoModal = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const [modalStyle] = useState(getModalStyle);
  const [title, setTitle] = useState("");
  const [status, setStatus] = useState("");
  const [description, setDescription] = useState("");
  const [selectedDate, setSelectedDate] = useState(new Date());

  const handleSubmit = (e) => {
    dispatch(addTodo({ title, status, description, selectedDate }));
    dispatch(closeModal(false));
  };
  return (
      <div style={modalStyle} className={classes.paper}>
        <form onSubmit={handleSubmit}>
          <Grid container justify="space-around">
            <TextField
              label="Title"
              variant="outlined"
              onChange={(e) => setTitle(e.target.value)}
            />
            <TextField
              label="Description"
              variant="outlined"
              onChange={(e) => setDescription(e.target.value)}
            />
            <FormControl variant="outlined">
            <InputLabel>Status</InputLabel>
            <Select onChange={(e) => setStatus(e.target.value)} label="Status">
              <MenuItem value={"To Do"}>To do</MenuItem>
              <MenuItem value={"In Progress"}>In Progress</MenuItem>
              <MenuItem value={"Done"}>Done</MenuItem>
            </Select>
          </FormControl>
          </Grid>

          <MuiPickersUtilsProvider utils={DateFnsUtils}>
            <Grid container justify="space-around">
              <KeyboardDatePicker
                value={selectedDate}
                format="MM/dd/yyyy"
                onChange={(date) => setSelectedDate(date)}
              />
            </Grid>
          </MuiPickersUtilsProvider>
          <Button variant="contained" color="primary" onClick={handleSubmit}>
            Submit
          </Button>
        </form>
      </div>
  );
};

export default ToDoModal;
