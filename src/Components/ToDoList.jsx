import React from "react";
import { Buttons } from "./Buttons";
import { Card, CardContent, CardHeader } from "@material-ui/core";
import { useSelector, useDispatch } from "react-redux";
import { removeTodo, markTodo } from "../Redux/todoListSlice";

const ToDoList = () => {
  const dispatch = useDispatch();
  const todos = useSelector((state) => state.todo);
  return (
    <div>
      {todos &&
        todos.map((todo, index) => (
          <Card variant="outlined">
            <CardHeader>
              <p>Title: {todo.title}</p>
            </CardHeader>
            <CardContent>
              <p>Description: {todo.description}</p>
              <p>Status:{todo.status}</p>
              <p>Date: {todo.selectedDate.toLocaleDateString(undefined, [])}</p>
              <Buttons
                key={index}
                index={index}
                todo={todo}
                markTodo={() => dispatch(markTodo({ index }))}
                removeTodo={() => dispatch(removeTodo({ index }))}
              />
            </CardContent>
          </Card>
        ))}
    </div>
  );
};

export default ToDoList;
