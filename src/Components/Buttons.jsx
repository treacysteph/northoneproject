import React from 'react';
import { Button } from '@material-ui/core';

export function Buttons({ todo, index, markTodo, removeTodo }) {
    return (
      <div>
        <span style={{ textDecoration: todo.isDone ? "line-through" : "" }}>{todo.text}</span>
        <div>
          <Button variant="outline-success" onClick={() => markTodo(index)}>✓</Button>{' '}
          <Button variant="outline-danger" onClick={() => removeTodo(index)}>✕</Button>
        </div>
      </div>
    );
  }