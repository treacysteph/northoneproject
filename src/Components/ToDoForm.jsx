import React from "react";
import ToDoList from "./ToDoList";
import ToDoModal from "./Modal";
import { Modal, Button } from "@material-ui/core";
import { useSelector, useDispatch } from "react-redux";
import { openModal, closeModal } from "../Redux/modalSlice";

const ToDoForm = () =>  {
  const dispatch = useDispatch();
  const modalStatus = useSelector((state) => state.modalStatus);
  console.log(modalStatus);

  return (
    <div>
      <Button variant="contained" color="primary" onClick={() => dispatch(openModal())}>
        Add Task
      </Button>
      <Modal open={modalStatus.value} onClose={() => dispatch(closeModal())}>
      <ToDoModal/>
      </Modal>
      <ToDoList />
    </div>
  );
}

export default ToDoForm;