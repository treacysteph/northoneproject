import { createSlice } from "@reduxjs/toolkit";


const initialState = { value: false }

export const modalStatus = createSlice({
  name: "modalStatus",
  initialState,
  reducers: {
    closeModal(state) {
      state.value = false
    },
    openModal(state) {
      state.value = true
    },
  },
});
export const { closeModal, openModal } = modalStatus.actions;

export default modalStatus.reducer;

