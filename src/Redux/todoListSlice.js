import { createSlice } from "@reduxjs/toolkit";

const initialState = [];

export const todo = createSlice({
  name: "todo",
  initialState,
  reducers: {
    addTodo: (state, action) => {
      state.push(action.payload);
    },
    markTodo: (state, action) => {
      const { index } = action.payload;
      const newState = state.map((todo, idx) => {
        if (idx === index) {
          return {
            ...todo,
            done: true,
            status: "Done",
          };
        }
        return todo;
      });
      return newState;
    },
    removeTodo: (state, action) => {
      const { index } = action.payload;
      state.splice(index, 1);
    },
  },
});

export const { addTodo, removeTodo, markTodo } = todo.actions;

export default todo.reducer;

