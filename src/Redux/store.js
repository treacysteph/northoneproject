import { combineReducers, configureStore } from '@reduxjs/toolkit';
import todoReducer  from './todoListSlice';
import modalStatusReducer from './modalSlice';

const reducer = combineReducers({
  todo: todoReducer,
  modalStatus: modalStatusReducer,
})

export default configureStore({
  reducer
});
